package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testGetFirstThrow(){
		Frame frame= new Frame(1,2);
		
		assertEquals(1,frame.getFirstThrow());
	}
	
	@Test
	public void testGetSecondThrow(){
		Frame frame= new Frame(1,2);
		
		assertEquals(2,frame.getSecondThrow());
	}
	
	@Test
	public void testGetScore(){
		Frame frame= new Frame(1,2);
		
		assertEquals(3,frame.getScore());
	}
	

}
