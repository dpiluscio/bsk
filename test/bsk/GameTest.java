package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void testGame() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));

		
		assertEquals(1,game.getFrameAt(0).getFirstThrow());
	}
	
	@Test
	public void testAddFrame() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1, 5));
	
		assertEquals(1,game.getFrameAt(0).getFirstThrow());
	}

	@Test
	public void testCalculateScoreAllStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);

		
		assertEquals(300,game.calculateScore());
	}
	
	@Test
	public void testCalculateScore() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		
		assertEquals(81,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		
		assertEquals(88,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		
		assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeAndSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		
		assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		
		assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithLastThrowSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(4,6));
		game.setFirstBonusThrow(7);
		
		assertEquals(90,game.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithLastThrowStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);

		
		assertEquals(92,game.calculateScore());
	}

}
