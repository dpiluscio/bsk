package tdd.training.bsk;

public class Game {
	private Frame[] bowlingGame= new Frame[10];
	private int counter=0;
	private int firstBonusT;
	private int secondBonusT;
	private int sum=0;

	/**
	 * It initializes an empty bowling game.
	 * @throws BowlingException 
	 */
	public Game() throws BowlingException {
		for(int i=0;i<bowlingGame.length;i++) {
			bowlingGame[i]= new Frame(0,0);
		}
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) {
		bowlingGame[counter]=frame;
		counter++;
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		return bowlingGame[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) {
		firstBonusT= firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) {
		secondBonusT= secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusT;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusT;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		boolean spare=false;
		boolean strike=false;
		int bonus=0;
		int counterStrike=0;
		for(int i=0;i<bowlingGame.length;i++) {
			sum+= bowlingGame[i].getScore();
			if(i < 9) {
				if(spare) {
					bowlingGame[i-1].setBonus(bowlingGame[i].getFirstThrow());
					sum+= bowlingGame[i-1].getBonus();
					spare=false;
					}
				if(strike) {
					bowlingGame[i-1].setBonus(bowlingGame[i].getScore());
					bonus= bowlingGame[i-1].getBonus();
					if(bowlingGame[i].isStrike()){
						bonus= 10+bowlingGame[i+1].getFirstThrow();
						bowlingGame[i].setBonus(bonus);
					}
					sum+= bonus;
					strike=false;
				}
				if(bowlingGame[i].isSpare()) {
					spare=true;
				}
				if(bowlingGame[i].isStrike()) {
					strike=true;
					counterStrike++;
				}
			}
			else {
				if(bowlingGame[i].isSpare()) {
					sum+= getFirstBonusThrow();
				}
				if(bowlingGame[i].isStrike()) {
					counterStrike++;
					if(getFirstBonusThrow()+getSecondBonusThrow()==20) {
						counterStrike+=2;
					}
					sum+= getFirstBonusThrow()+getSecondBonusThrow();
				}
			}
			
			
		}
		if(counterStrike==12) {
			return 300;
		}
		else {
			return sum;	
		}
		
	}

}
